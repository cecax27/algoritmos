import os
import datetime

global listaPersonas 
listaPersonas = []

global listaVehiculos
listaVehiculos = []

barra = "------------------------\n"

global tipoPersona
tipoPersona = {}
tipoPersona[1]= "Administrativo"
tipoPersona[2]= "Enfermero"
tipoPersona[3]= "Doctor"
tipoPersona[4]= "Paciente"

global tipoVehiculo
tipoVehiculo = {}
tipoVehiculo[1]= "Auto compacto"
tipoVehiculo[2]= "Ambulancia"
tipoVehiculo[3]= "Tractocamión"
tipoVehiculo[4]= "Camioneta"
tipoVehiculo[5]= "Motocicleta"

global tipoTransmision
tipoTransmision = {}
tipoTransmision[1] = "Estándar"
tipoTransmision[2] = "Automática"
tipoTransmision[3] = "Semi-aútomática"


class Vehiculo:
	placa = ""
	tipo = 0
	fechaRegistro = datetime.datetime
	marca = ""
	modelo = ""
	rodado = 0
	numLlantas = 0
	transmision = 0
	cilindraje = 0
	numPuertas = 0
	asignado = 0		

class Persona:
	clave = 0
	tipo = 0
	nombre = ""
	fechaNacimiento = datetime.datetime
	fechaRegistro = datetime.datetime
	curp = ""
	rfc = ""
	tipoSangre = ""
	padecimientos = ""
	estupefacientes = ""
	enfermedades = ""

def registrarPersona():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Registro de Persona")
	new = Persona()
	while new.tipo < 1 or new.tipo > 4:
		print("Tipo:")
		print("1. Administrativo")
		print("2. Enfermero")
		print("3. Doctor")
		print("4. Paciente")
		new.tipo = int(input("Tipo: "))
	new.nombre = input("Nombre:")
	print("Fecha de nacimiento:")
	a = 0
	m = 0
	d = 0
	while a < 1918 or a > 2018:
		a = int(input("Año (ej. 1998): "))
	while  m < 1 or m > 12:
		m = int(input("Mes (ej. 10): "))
	while  d < 1 or d > 31:
		d = int(input("Día (ej. 27): "))
	new.fechaNacimiento = datetime.datetime(a,m,d)
	new.fechaRegistro = datetime.datetime.now()
	new.curp = input("C.U.R.P.: ")
	new.rfc = input("R.F.C.:")
	new.tipoSangre = input("Tipo de Sangre: ")
	new.padecimientos = input("Padecimientos: ")
	new.estupefacientes = input("Consume estupefacientes?: ")
	new.enfermedades = input("Enfermedades:")
	new.clave = (new.tipo*10000)+((new.fechaNacimiento.year % 100)*100)
	for i in listaPersonas:
		if(i.clave==new.clave):
			new.clave=new.clave+1
	print("Clave: ",new.clave)
	os.system("pause")
	listaPersonas.append(new)

def registrarVehiculo():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Registro de Vehículo")
	new = Vehiculo()
	print("Tipo:")
	print("1. Auto compacto")
	print("2. Ambulancia")
	print("3. Tractocamión")
	print("4. Camioneta")
	print("5. Motocicleta")
	new.tipo = int(input("Tipo: "))
	band = False
	while not band:
		new.placa = input("Placas:")
		band = True
		for i in listaVehiculos:
			if new.placa == i.placa:
				band = False
				print("Placas ya registradas.\n\n")
	new.fechaRegistro = datetime.datetime.now()
	new.marca = input("Marca: ")
	new.modelo = input("Modelo:")
	new.rodado = int(input("Kilometros rodados: "))
	if new.tipo == 1 or new.tipo ==2 or new.tipo == 4:
		new.numLlantas = 4
	elif new.tipo == 5:
		new.numLlantas = 2
	elif new.tipo == 3:
		new.numLlantas = int(input("Número de llantas: "))
	print("Trasmisión:")
	print("1. Estándar")
	print("2. Automática")
	print("3. Semi-aútomática")
	new.transmision =int(input("-> "))
	new.cilindraje = int(input("Número de cilindros: "))
	if new.tipo == 5:
		new.numPuertas = 0
	else:
		new.numPuertas = int(input("Número de puertas: "))
	print("¿Asigar a persona?")
	print("1. Sí")
	print("2. No")
	asig = int(input("-> "))
	if asig == 1:
		band = False
		while not band:
			new.asignado = int(input("Clave de persona: "))
			for i in listaPersonas:
				if int(i.clave)==int(new.asignado):
					band = True
			if not band:
				print("Clave no encontrada.\n\n")
	else:
		new.asignado = 0
	os.system("pause")
	listaVehiculos.append(new)

def buscarPersona():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Búsqueda de persona")
	clave = input("Clave: ")
	encontrado = False
	for a in listaPersonas:
		if int(a.clave) == int(clave):
			print("\n[",a.clave,"]")
			print(tipoPersona[a.tipo])
			print(a.nombre+"\n")
			print("Fecha de nacimiento: "+str(a.fechaNacimiento))
			print("Fecha de registro: "+str(a.fechaRegistro))
			edad = (datetime.datetime.now()-a.fechaNacimiento)
			print("Edad: "+str(int(edad.days/365))+" años")
			print("C.U.R.P.: "+a.curp)
			print("R.F.C.: "+a.rfc)
			print("Tipo de sangre: "+a.tipoSangre)
			print("Padecimientos: "+a.padecimientos)
			print("Consume estupefacientes: "+a.estupefacientes)
			print("Enfermedades: "+a.enfermedades+"\n")
			os.system("pause")
			encontrado = True
	if not encontrado:
		print("Clave no encontrada.")
		os.system("pause")

def buscarVehiculo():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Búsqueda de Vehiculo")
	placa = input("Placas: ")
	encontrado = False
	for a in listaVehiculos:
		if a.placa == placa:
			print("\n["+a.placa+"]")
			print(tipoVehiculo[a.tipo])
			print("Fecha de registro: "+str(a.fechaRegistro))
			print("Marca: "+a.marca)
			print("Modelo: "+a.modelo)
			print("Kilometros rodados: "+str(a.rodado)+" km")
			print("Número de llantas: "+str(a.numLlantas))
			print("Tipo de transmision: "+tipoTransmision[a.transmision])
			print("Número de cilindros: "+str(a.cilindraje))
			print("Número de puertas: "+str(a.numPuertas))
			if a.asignado == 0:
				print("Vehículo no asignado.")
			else:
				for i in listaPersonas:
					if int(a.asignado) == int(i.clave):
						nombre = i.nombre
				print("Asignado a: "+nombre+" con clave "+str(a.asignado))
			os.system("pause")
			encontrado = True
	if not encontrado:
		print("Placa no encontrada.")
		os.system("pause")

def listarPersona():
	for a in listaPersonas:
		os.system("cls")
		print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
		print("Lista de personas")

		print("\n"+str(listaPersonas.index(a)+1)+" de "+str(len(listaPersonas)))

		print("\n[",a.clave,"]")
		print(tipoPersona[a.tipo])
		print(a.nombre+"\n")
		print("Fecha de nacimiento: "+str(a.fechaNacimiento))
		print("Fecha de registro: "+str(a.fechaRegistro))
		edad = (datetime.datetime.now()-a.fechaNacimiento)
		print("Edad: "+str(int(edad.days/365))+" años")
		print("C.U.R.P.: "+a.curp)
		print("R.F.C.: "+a.rfc)
		print("Tipo de sangre: "+a.tipoSangre)
		print("Padecimientos: "+a.padecimientos)
		print("Consume estupefacientes: "+a.estupefacientes)
		print("Enfermedades: "+a.enfermedades+"\n")
		os.system("pause")

	if len(listaPersonas) == 0:
		os.system("cls")
		print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
		print("Lista de personas")

		print("No hay personas registradas.")
		os.system("pause")

def listarVehiculo():
	for a in listaVehiculos:
		os.system("cls")
		print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
		print("Lista de Vehículos")

		print("\n"+str(listaVehiculos.index(a)+1)+" de "+str(len(listaVehiculos)))

		print("\n["+a.placa+"]")
		print(tipoVehiculo[a.tipo])
		print("Fecha de registro: "+str(a.fechaRegistro))
		print("Marca: "+a.marca)
		print("Modelo: "+a.modelo)
		print("Kilometros rodados: "+str(a.rodado)+" km")
		print("Número de llantas: "+str(a.numLlantas))
		print("Tipo de transmision: "+tipoTransmision[a.transmision])
		print("Número de cilindros: "+str(a.cilindraje))
		print("Número de puertas: "+str(a.numPuertas))
		if a.asignado == 0:
			print("Vehículo no asignado.")
		else:
			for i in listaPersonas:
				if int(a.asignado) == int(i.clave):
					nombre = i.nombre
			print("Asignado a: "+nombre+" con clave "+str(a.asignado))
		os.system("pause")

	if len(listaVehiculos) == 0:
		os.system("cls")
		print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
		print("Lista de Vehículos")

		print("No hay vehículos registradas.")
		os.system("pause")

def modificarPersona():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Modificar persona")
	clave = input("Clave: ")
	encontrado = False
	for a in listaPersonas:
		if int(a.clave) == int(clave):
			encontrado = True
			print("\n[",a.clave,"]")
			print(tipoPersona[a.tipo])
			print(a.nombre+"\n")
			print("Fecha de nacimiento: "+str(a.fechaNacimiento))
			print("Fecha de registro: "+str(a.fechaRegistro))
			edad = (datetime.datetime.now()-a.fechaNacimiento)
			print("Edad: "+str(int(edad.days/365))+" años")
			print("C.U.R.P.: "+a.curp)
			print("R.F.C.: "+a.rfc)
			print("Tipo de sangre: "+a.tipoSangre)
			print("Padecimientos: "+a.padecimientos)
			print("Consume estupefacientes: "+a.estupefacientes)
			print("Enfermedades: "+a.enfermedades+"\n")
			listaPersonas.pop(listaPersonas.index(a))
			new = Persona()
			print("Tipo:")
			print("1. Administrativo")
			print("2. Enfermero")
			print("3. Doctor")
			print("4. Paciente")
			new.tipo = int(input("Tipo: "))
			new.nombre = input("Nombre:")
			print("Fecha de nacimiento:")
			a = int(input("Año: "))
			m = int(input("Mes: "))
			d = int(input("Día: "))
			new.fechaNacimiento = datetime.datetime(a,m,d)
			new.fechaRegistro = datetime.datetime.now()
			new.curp = input("C.U.R.P.: ")
			new.rfc = input("R.F.C.:")
			new.tipoSangre = input("Tipo de Sangre: ")
			new.padecimientos = input("Padecimientos: ")
			new.estupefacientes = input("Consume estupefacientes?: ")
			new.enfermedades = input("Enfermedades:")
			new.clave = (new.tipo*10000)+((new.fechaNacimiento.year % 100)*100)
			for i in listaPersonas:
				if(i.clave==new.clave):
					new.clave=new.clave+1
			print("Nueva clave: ",new.clave)
			os.system("pause")
			listaPersonas.append(new)
	if not encontrado:
		print("Clave no encontrada.")
		os.system("pause")

def modificarVehiculo():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Modificar Vehiculo")
	placa = input("Placas: ")
	encontrado = False
	for a in listaVehiculos:
		if a.placa == placa:
			encontrado = True
			print("\n["+a.placa+"]")
			print(tipoVehiculo[a.tipo])
			print("Fecha de registro: "+str(a.fechaRegistro))
			print("Marca: "+a.marca)
			print("Modelo: "+a.modelo)
			print("Kilometros rodados: "+str(a.rodado)+" km")
			print("Número de llantas: "+str(a.numLlantas))
			print("Tipo de transmision: "+tipoTransmision[a.transmision])
			print("Número de cilindros: "+str(a.cilindraje))
			print("Número de puertas: "+str(a.numPuertas))
			if a.asignado == 0:
				print("Vehículo no asignado.")
			else:
				for i in listaPersonas:
					if int(a.asignado) == int(i.clave):
						nombre = i.nombre
				print("Asignado a: "+nombre+" con clave "+str(a.asignado))
			listaVehiculos.pop(listaVehiculos.index(a))
			new = Vehiculo()
			print("Tipo:")
			print("1. Auto compacto")
			print("2. Ambulancia")
			print("3. Tractocamión")
			print("4. Camioneta")
			print("5. Motocicleta")
			new.tipo = int(input("Tipo: "))
			band = False
			while not band:
				new.placa = input("Placas:")
				band = True
				for i in listaVehiculos:
					if new.placa == i.placa:
						band = False
						print("Placas ya registradas.\n\n")
			new.fechaRegistro = datetime.datetime.now()
			new.marca = input("Marca: ")
			new.modelo = input("Modelo:")
			new.rodado = int(input("Kilometros rodados: "))
			if new.tipo == 1 or new.tipo ==2 or new.tipo == 4:
				new.numLlantas = 4
			elif new.tipo == 5:
				new.numLlantas = 2
			elif new.tipo == 3:
				new.numLlantas = int(input("Número de llantas: "))
			print("Trasmisión:")
			print("1. Estándar")
			print("2. Automática")
			print("3. Semi-aútomática")
			new.transmision =int(input("-> "))
			new.cilindraje = int(input("Número de cilindros: "))
			if new.tipo == 5:
				new.numPuertas = 0
			else:
				new.numPuertas = int(input("Número de puertas: "))
			print("¿Asigar a persona?")
			print("1. Sí")
			print("2. No")
			asig = int(input("-> "))
			if asig == 1:
				band = False
				while not band:
					new.asignado = int(input("Clave de persona: "))
					for i in listaPersonas:
						if int(i.clave)==int(new.asignado):
							band = True
					if not band:
						print("Clave no encontrada.\n\n")
			else:
				new.asignado = 0
			os.system("pause")
			listaVehiculos.append(new)
	if not encontrado:
		print("Placa no encontrada.")
		os.system("pause")

def asignarVehiculo():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Asignar Vehiculo")
	placa = input("Placas: ")
	encontrado = False
	for a in listaVehiculos:
		if a.placa == placa:
			encontrado = True
			print("\n["+a.placa+"]")
			print(tipoVehiculo[a.tipo])
			print("Fecha de registro: "+str(a.fechaRegistro))
			print("Marca: "+a.marca)
			print("Modelo: "+a.modelo)
			print("Kilometros rodados: "+str(a.rodado)+" km")
			print("Número de llantas: "+str(a.numLlantas))
			print("Tipo de transmision: "+tipoTransmision[a.transmision])
			print("Número de cilindros: "+str(a.cilindraje))
			print("Número de puertas: "+str(a.numPuertas))
			if a.asignado == 0:
				print("Vehículo no asignado.")
			else:
				for i in listaPersonas:
					if int(a.asignado) == int(i.clave):
						nombre = i.nombre
				print("Asignado a: "+nombre+" con clave "+str(a.asignado))
			print("¿Asigar a persona?")
			print("1. Sí")
			print("2. No")
			asig = int(input("-> "))
			if asig == 1:
				band = False
				while not band:
					a.asignado = int(input("Clave de persona: "))
					for i in listaPersonas:
						if int(i.clave)==int(a.asignado):
							band = True
					if not band:
						print("Clave no encontrada.\n\n")
			else:
				a.asignado = 0
			os.system("pause")
	if not encontrado:
		print("Placa no encontrada.")
		os.system("pause")

def bajaPersona():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Baja de persona")
	clave = input("Clave: ")
	encontrado = False
	for a in listaPersonas:
		if int(a.clave) == int(clave):
			print("\n[",a.clave,"]")
			print(tipoPersona[a.tipo])
			print(a.nombre+"\n")
			print("Fecha de nacimiento: "+str(a.fechaNacimiento))
			print("Fecha de registro: "+str(a.fechaRegistro))
			edad = (datetime.datetime.now()-a.fechaNacimiento)
			print("Edad: "+str(int(edad.days/365))+" años")
			print("C.U.R.P.: "+a.curp)
			print("R.F.C.: "+a.rfc)
			print("Tipo de sangre: "+a.tipoSangre)
			print("Padecimientos: "+a.padecimientos)
			print("Consume estupefacientes: "+a.estupefacientes)
			print("Enfermedades: "+a.enfermedades+"\n")
			encontrado = True
			print("¿Eliminar?\n1. Sí\n2.No")
			eli = int(input("->"))
			if eli == 1:
				listaPersonas.pop(listaPersonas.index(a))
	if not encontrado:
		print("Clave no encontrada.")
		os.system("pause")

def bajaVehiculo():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Baja de Vehiculo")
	placa = input("Placas: ")
	encontrado = False
	for a in listaVehiculos:
		if a.placa == placa:
			print("\n["+a.placa+"]")
			print(tipoVehiculo[a.tipo])
			print("Fecha de registro: "+str(a.fechaRegistro))
			print("Marca: "+a.marca)
			print("Modelo: "+a.modelo)
			print("Kilometros rodados: "+str(a.rodado)+" km")
			print("Número de llantas: "+str(a.numLlantas))
			print("Tipo de transmision: "+tipoTransmision[a.transmision])
			print("Número de cilindros: "+str(a.cilindraje))
			print("Número de puertas: "+str(a.numPuertas))
			if a.asignado == 0:
				print("Vehículo no asignado.")
			else:
				for i in listaPersonas:
					if int(a.asignado) == int(i.clave):
						nombre = i.nombre
				print("Asignado a: "+nombre+" con clave "+str(a.asignado))
			os.system("pause")
			encontrado = True
			print("¿Eliminar?\n1. Sí\n2.No")
			eli = int(input("->"))
			if eli == 1:
				listaVehiculos.pop(listaVehiculos.index(a))
	if not encontrado:
		print("Placa no encontrada.")
		os.system("pause")

def empleados():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Empleados entre 30 y 40 años")
	emple = []
	for a in listaPersonas:
		edad = (datetime.datetime.now()-a.fechaNacimiento)
		edad = int(edad.days/365)
		if edad <= 40 and edad >= 30 and a.tipo!=4:
			emple.append(a)
	print("\n"+str(len(emple))+" empleados encontrados.")
	os.system("pause")
	for a in emple:
		os.system("cls")
		print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
		print("Empleados entre 30 y 40 años")

		print("\n"+str(emple.index(a)+1)+" de "+str(len(emple)))

		print("\n[",a.clave,"]")
		print(tipoPersona[a.tipo])
		print(a.nombre+"\n")
		print("Fecha de nacimiento: "+str(a.fechaNacimiento))
		print("Fecha de registro: "+str(a.fechaRegistro))
		edad = (datetime.datetime.now()-a.fechaNacimiento)
		print("Edad: "+str(int(edad.days/365))+" años")
		print("C.U.R.P.: "+a.curp)
		print("R.F.C.: "+a.rfc)
		print("Tipo de sangre: "+a.tipoSangre)
		print("Padecimientos: "+a.padecimientos)
		print("Consume estupefacientes: "+a.estupefacientes)
		print("Enfermedades: "+a.enfermedades+"\n")
		os.system("pause")

def comprobacion():
	texto = "clave,nombre,fechaNacimiento,fechaRegistro,curp,rfc,tipoSangre,padecimientos,estupefacientes,enfermedades\n"

	if not os.path.exists("datos/"):
	  os.mkdir("datos/")

	if not os.path.exists("datos/administrativo.csv"):
		with open("datos/administrativo.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/enfermero.csv"):
		with open("datos/enfermero.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/doctor.csv"):
		with open("datos/doctor.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/paciente.csv"):
		with open("datos/paciente.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()

	texto = "placa,tipo,fechaRegistro,marca,modelo,rodado,numLlantas,transmision,cilindraje,numPuertas,asignado\n"

	if not os.path.exists("datos/autocompacto.csv"):
		with open("datos/autocompacto.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/ambulancia.csv"):
		with open("datos/ambulancia.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/tractocamion.csv"):
		with open("datos/tractocamion.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/camioneta.csv"):
		with open("datos/camioneta.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()
	if not os.path.exists("datos/motocicleta.csv"):
		with open("datos/motocicleta.csv",'w') as archivo:
			archivo.write(texto)
		archivo.close()

def leerBases():
	with open("datos/administrativo.csv",'r') as admin:
		lineas=admin.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Persona()
			new.tipo = 1
			new.clave = linea[0]
			new.nombre = linea[1]
			fechaNacimiento1 = linea[2]
			fechaNacimiento = fechaNacimiento1.split('-')
			fechaNacimiento[2] = fechaNacimiento[2][0:2]
			new.fechaNacimiento = datetime.datetime(int(fechaNacimiento[0]),int(fechaNacimiento[1]),int(fechaNacimiento[2]))
			new.fechaRegistro = linea[3]
			new.curp = linea[4]
			new.rfc = linea[5]
			new.tipoSangre = linea[6]
			new.padecimientos = linea[7]
			new.estupefacientes = linea[8]
			new.enfermedades = linea[9]
			listaPersonas.append(new)
	admin.close()
	with open("datos/enfermero.csv",'r') as enfer:
		lineas=enfer.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Persona()
			new.tipo = 2
			new.clave = linea[0]
			new.nombre = linea[1]
			fechaNacimiento1 = linea[2]
			fechaNacimiento = fechaNacimiento1.split('-')
			fechaNacimiento[2] = fechaNacimiento[2][0:2]
			new.fechaNacimiento = datetime.datetime(int(fechaNacimiento[0]),int(fechaNacimiento[1]),int(fechaNacimiento[2]))
			new.fechaRegistro = linea[3]
			new.curp = linea[4]
			new.rfc = linea[5]
			new.tipoSangre = linea[6]
			new.padecimientos = linea[7]
			new.estupefacientes = linea[8]
			new.enfermedades = linea[9]
			listaPersonas.append(new)
	enfer.close()
	with open("datos/doctor.csv",'r') as doctor:
		lineas=doctor.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Persona()
			new.tipo = 3
			new.clave = linea[0]
			new.nombre = linea[1]
			fechaNacimiento1 = linea[2]
			fechaNacimiento = fechaNacimiento1.split('-')
			fechaNacimiento[2] = fechaNacimiento[2][0:2]
			new.fechaNacimiento = datetime.datetime(int(fechaNacimiento[0]),int(fechaNacimiento[1]),int(fechaNacimiento[2]))
			new.fechaRegistro = linea[3]
			new.curp = linea[4]
			new.rfc = linea[5]
			new.tipoSangre = linea[6]
			new.padecimientos = linea[7]
			new.estupefacientes = linea[8]
			new.enfermedades = linea[9]
			listaPersonas.append(new)
	doctor.close()
	with open("datos/paciente.csv",'r') as pacient:
		lineas=pacient.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Persona()
			new.tipo = 4
			new.clave = linea[0]
			new.nombre = linea[1]
			fechaNacimiento1 = linea[2]
			fechaNacimiento = fechaNacimiento1.split('-')
			fechaNacimiento[2] = fechaNacimiento[2][0:2]
			new.fechaNacimiento = datetime.datetime(int(fechaNacimiento[0]),int(fechaNacimiento[1]),int(fechaNacimiento[2]))
			new.fechaRegistro = linea[3]
			new.curp = linea[4]
			new.rfc = linea[5]
			new.tipoSangre = linea[6]
			new.padecimientos = linea[7]
			new.estupefacientes = linea[8]
			new.enfermedades = linea[9]
			listaPersonas.append(new)
	pacient.close()
	with open("datos/autocompacto.csv",'r') as auto:
		lineas=auto.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Vehiculo()
			new.placa = linea[0]
			new.tipo = int(linea[1])
			new.fechaRegistro = linea[2]
			new.marca = linea[3]
			new.modelo = linea[4]
			new.rodado = int(linea[5])
			new.numLlantas = int(linea[6])
			new.transmision = int(linea[7])
			new.cilindraje = int(linea[8])
			new.numPuertas = int(linea[9])
			new.asignado = int(linea[10])
			listaVehiculos.append(new)
	auto.close()
	with open("datos/ambulancia.csv",'r') as ambul:
		lineas=ambul.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Vehiculo()
			new.placa = linea[0]
			new.tipo = int(linea[1])
			new.fechaRegistro = linea[2]
			new.marca = linea[3]
			new.modelo = linea[4]
			new.rodado = int(linea[5])
			new.numLlantas = int(linea[6])
			new.transmision = int(linea[7])
			new.cilindraje = int(linea[8])
			new.numPuertas = int(linea[9])
			new.asignado = int(linea[10])
			listaVehiculos.append(new)
	ambul.close()
	with open("datos/tractocamion.csv",'r') as tracto:
		lineas=tracto.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Vehiculo()
			new.placa = linea[0]
			new.tipo = int(linea[1])
			new.fechaRegistro = linea[2]
			new.marca = linea[3]
			new.modelo = linea[4]
			new.rodado = int(linea[5])
			new.numLlantas = int(linea[6])
			new.transmision = int(linea[7])
			new.cilindraje = int(linea[8])
			new.numPuertas = int(linea[9])
			new.asignado = int(linea[10])
			listaVehiculos.append(new)
	tracto.close()
	with open("datos/camioneta.csv",'r') as camion:
		lineas=camion.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Vehiculo()
			new.placa = linea[0]
			new.tipo = int(linea[1])
			new.fechaRegistro = linea[2]
			new.marca = linea[3]
			new.modelo = linea[4]
			new.rodado = int(linea[5])
			new.numLlantas = int(linea[6])
			new.transmision = int(linea[7])
			new.cilindraje = int(linea[8])
			new.numPuertas = int(linea[9])
			new.asignado = int(linea[10])
			listaVehiculos.append(new)
	camion.close()
	with open("datos/motocicleta.csv",'r') as moto:
		lineas=moto.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			new = Vehiculo()
			new.placa = linea[0]
			new.tipo = int(linea[1])
			new.fechaRegistro = linea[2]
			new.marca = linea[3]
			new.modelo = linea[4]
			new.rodado = int(linea[5])
			new.numLlantas = int(linea[6])
			new.transmision = int(linea[7])
			new.cilindraje = int(linea[8])
			new.numPuertas = int(linea[9])
			new.asignado = int(linea[10])
			listaVehiculos.append(new)
	moto.close()

def escribirBases():
	with open("datos/administrativo.csv",'w') as admin:
		with open("datos/enfermero.csv",'w') as enfer:
			with open("datos/doctor.csv",'w') as doctor:
				with open("datos/paciente.csv",'w') as pacient:
					admin.write("clave,nombre,fechaNacimiento,fechaRegistro,curp,rfc,tipoSangre,padecimientos,estupefacientes,enfermedades\n")
					enfer.write("clave,nombre,fechaNacimiento,fechaRegistro,curp,rfc,tipoSangre,padecimientos,estupefacientes,enfermedades\n")
					doctor.write("clave,nombre,fechaNacimiento,fechaRegistro,curp,rfc,tipoSangre,padecimientos,estupefacientes,enfermedades\n")
					pacient.write("clave,nombre,fechaNacimiento,fechaRegistro,curp,rfc,tipoSangre,padecimientos,estupefacientes,enfermedades\n")
					for v in listaPersonas:
						if v.tipo == 1:
							admin.write(str(v.clave)+",")
							admin.write(v.nombre+",")
							admin.write(str(v.fechaNacimiento)+",")
							admin.write(str(v.fechaRegistro)+",")
							admin.write(v.curp+",")
							admin.write(v.rfc+",")
							admin.write(v.tipoSangre+",")
							admin.write(v.padecimientos+",")
							admin.write(v.estupefacientes+",")
							admin.write(v.enfermedades+"\n")
						if v.tipo == 2:
							enfer.write(str(v.clave)+",")
							enfer.write(v.nombre+",")
							enfer.write(str(v.fechaNacimiento)+",")
							enfer.write(str(v.fechaRegistro)+",")
							enfer.write(v.curp+",")
							enfer.write(v.rfc+",")
							enfer.write(v.tipoSangre+",")
							enfer.write(v.padecimientos+",")
							enfer.write(v.estupefacientes+",")
							enfer.write(v.enfermedades+"\n")
						if v.tipo == 3:
							doctor.write(str(v.clave)+",")
							doctor.write(v.nombre+",")
							doctor.write(str(v.fechaNacimiento)+",")
							doctor.write(str(v.fechaRegistro)+",")
							doctor.write(v.curp+",")
							doctor.write(v.rfc+",")
							doctor.write(v.tipoSangre+",")
							doctor.write(v.padecimientos+",")
							doctor.write(v.estupefacientes+",")
							doctor.write(v.enfermedades+"\n")
						if v.tipo == 4:
							pacient.write(str(v.clave)+",")
							pacient.write(v.nombre+",")
							pacient.write(str(v.fechaNacimiento)+",")
							pacient.write(str(v.fechaRegistro)+",")
							pacient.write(v.curp+",")
							pacient.write(v.rfc+",")
							pacient.write(v.tipoSangre+",")
							pacient.write(v.padecimientos+",")
							pacient.write(v.estupefacientes+",")
							pacient.write(v.enfermedades+"\n")
				pacient.close()
			doctor.close()
		enfer.close()
	admin.close()
	with open("datos/autocompacto.csv",'w') as auto:
		with open("datos/ambulancia.csv",'w') as ambul:
			with open("datos/tractocamion.csv",'w') as tracto:
				with open("datos/camioneta.csv",'w') as camion:
					with open("datos/motocicleta.csv", 'w') as moto:
						auto.write("placa,tipo,fechaRegistro,marca,modelo,rodado,numLlantas,transmision,cilindraje,numPuertas,asignado\n")
						ambul.write("placa,tipo,fechaRegistro,marca,modelo,rodado,numLlantas,transmision,cilindraje,numPuertas,asignado\n")
						tracto.write("placa,tipo,fechaRegistro,marca,modelo,rodado,numLlantas,transmision,cilindraje,numPuertas,asignado\n")
						camion.write("placa,tipo,fechaRegistro,marca,modelo,rodado,numLlantas,transmision,cilindraje,numPuertas,asignado\n")
						moto.write("placa,tipo,fechaRegistro,marca,modelo,rodado,numLlantas,transmision,cilindraje,numPuertas,asignado\n")
						for v in listaVehiculos:
							if v.tipo == 1:
								auto.write(str(v.placa)+",")
								auto.write(str(v.tipo)+",")
								auto.write(str(v.fechaRegistro)+",")
								auto.write(v.marca+",")
								auto.write(v.modelo+",")
								auto.write(str(v.rodado)+",")
								auto.write(str(v.numLlantas)+",")
								auto.write(str(v.transmision)+",")
								auto.write(str(v.cilindraje)+",")
								auto.write(str(v.numPuertas)+",")
								auto.write(str(v.asignado)+"\n")
							if v.tipo == 2:
								ambul.write(str(v.placa)+",")
								ambul.write(str(v.tipo)+",")
								ambul.write(str(v.fechaRegistro)+",")
								ambul.write(v.marca+",")
								ambul.write(v.modelo+",")
								ambul.write(str(v.rodado)+",")
								ambul.write(str(v.numLlantas)+",")
								ambul.write(str(v.transmision)+",")
								ambul.write(str(v.cilindraje)+",")
								ambul.write(str(v.numPuertas)+",")
								ambul.write(str(v.asignado)+"\n")
							if v.tipo == 3:
								tracto.write(str(v.placa)+",")
								tracto.write(str(v.tipo)+",")
								tracto.write(str(v.fechaRegistro)+",")
								tracto.write(v.marca+",")
								tracto.write(v.modelo+",")
								tracto.write(str(v.rodado)+",")
								tracto.write(str(v.numLlantas)+",")
								tracto.write(str(v.transmision)+",")
								tracto.write(str(v.cilindraje)+",")
								tracto.write(str(v.numPuertas)+",")
								tracto.write(str(v.asignado)+"\n")
							if v.tipo == 4:
								camion.write(str(v.placa)+",")
								camion.write(str(v.tipo)+",")
								camion.write(str(v.fechaRegistro)+",")
								camion.write(v.marca+",")
								camion.write(v.modelo+",")
								camion.write(str(v.rodado)+",")
								camion.write(str(v.numLlantas)+",")
								camion.write(str(v.transmision)+",")
								camion.write(str(v.cilindraje)+",")
								camion.write(str(v.numPuertas)+",")
								camion.write(str(v.asignado)+"\n")
							if v.tipo == 5:
								moto.write(str(v.placa)+",")
								moto.write(str(v.tipo)+",")
								moto.write(str(v.fechaRegistro)+",")
								moto.write(v.marca+",")
								moto.write(v.modelo+",")
								moto.write(str(v.rodado)+",")
								moto.write(str(v.numLlantas)+",")
								moto.write(str(v.transmision)+",")
								moto.write(str(v.cilindraje)+",")
								moto.write(str(v.numPuertas)+",")
								moto.write(str(v.asignado)+"\n")
					moto.close()
				camion.close()
			tracto.close()
		ambul.close()
	auto.close()	

def salir():
	os.system("cls")
	print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
	print("Salir")
	escribirBases()

def menu():
	op = 0
	while op != 6:
		os.system("cls")
		print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
		print("Menú")
		print("1. Alta de personas/vehículos")
		print("2. Búsqueda de personas/vehículos")
		print("3. Modificar datos de personas/vehículos")
		print("4. Eliminar personas/vehículos")
		print("5. Mostrar empleados entre 30 y 40 años")
		print("6. Salir")
		op = int(input("Opción: "))

		if op == 1:
			os.system("cls")
			print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
			print("Alta")
			print("1. Alta de persona")
			print("2. Alta de vehículo")
			op2 = int(input("Opción: "))
			if op2 == 1 :
				registrarPersona()
			elif op2 == 2:
				registrarVehiculo()
		elif op == 2:
			os.system("cls")
			print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
			print("Búsqueda")
			print("1. Búsqueda de persona")
			print("2. Búsqueda de vehículo")
			op2 = int(input("Opción: "))
			if op2 == 1 :
				os.system("cls")
				print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
				print("Búsqueda de persona")
				print("1. Búsqueda de persona")
				print("2. Listar todas las personas")
				op3 = int(input("Opción: "))
				if op3 == 1 :
					buscarPersona()
				elif op3 == 2:
					listarPersona()
			elif op2 == 2:
				os.system("cls")
				print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
				print("Búsqueda de vehículo")
				print("1. Búsqueda de vehículo")
				print("2. Listar todos los vehículos")
				op3 = int(input("Opción: "))
				if op3 == 1 :
					buscarVehiculo()
				elif op3 == 2:
					listarVehiculo()
		elif op == 3:
			os.system("cls")
			print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
			print("Modificación de datos")
			print("1. Modificación de datos de persona")
			print("2. Modificación de datos de vehículo")
			op2 = int(input("Opción: "))
			if op2 == 1 :
				modificarPersona()
			elif op2 == 2:
				os.system("cls")
				print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
				print("Modificación de datos de vehiculo")
				print("1. Modificación de datos")
				print("2. Asignar vehículo")
				op3 = int(input("Opción: "))
				if op3 == 1 :
					modificarVehiculo()
				elif op3 == 2:
					asignarVehiculo()
		elif op == 4:
			os.system("cls")
			print(barra+"Clínica San Juan XXIII\n"+barra+"\n")
			print("Baja")
			print("1. Baja de persona")
			print("2. Baja de vehículo")
			op2 = int(input("Opción: "))
			if op2 == 1 :
				bajaPersona()
			elif op2 == 2:
				bajaVehiculo()
		elif op == 5:
			empleados()
		elif op == 6:
			salir()
		else:
			print("Seleccione una opción correcta")

comprobacion()
leerBases()
menu()