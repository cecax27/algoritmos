import os

class UI:
	errores = {1: "Opción no encontrada", 2: "Archivo no encontrado", 3: "Archivo con formato incorrecto", 4: "ID no encontrado", 5: "Número introducido fuera de rango"}

	def encabezado(self, string):
		os.system("cls")
		print("------------------------\n")
		print("     Robotic's Mex\n")
		print("------------------------\n")
		print("  "+string+"\n")

	def error(self, num):
		print("\n\n####")
		print("Error "+str(num))
		print(str(self.errores[num]))
		os.system("pause")

	def info(self):
		self.encabezado("Información")
		print("Programa hecho por:")
		print("Carlos Cervantes Aguilar")
		print("Estudiante de Ing. Mecatrónica")
		print("Código 214717889\n")
		print("Universidad de Guadalajara")
		print("Centro Universitario de los Lagos\n")
		print("Práctica 2 de Algoritmos y Estructuras de Datos")
		print("Profesor Víctor Becerra Córdoba\n")
		os.system("pause")

datos = []
resultado = []
interfaz = UI()
nombre = ""
while not os.path.exists(nombre):
	interfaz.encabezado("Procesar archivo")
	print("i. Este programa lee un archivo .csv, procesa los datos y guarda en la misma carpeta el archivo \"resultadopy.csv\" con la clasficiación por clases.\n\n")
	nombre = str(input("Nombre del documento csv: "))+".csv"
	print("Procesando...\n")
with open(nombre,'r') as archivo:
	lineas=archivo.read().splitlines()
	lineas.pop(0)
	for l in lineas:
		linea = l.split(',')
		datos.append([int(linea[0]),int(linea[1]),int(linea[2]),int(linea[3]),int(linea[4]),int(linea[5]),int(linea[6]),int(linea[7]),int(linea[8]),int(linea[9])])
archivo.close();
for v in datos:
	reslin = []
	for e in v:
		if e <=200:
			reslin.append('FMR')
		elif e <=500:
			reslin.append('FM')
		elif e <=799:
			reslin.append('FA')
		elif e <=1000:
			reslin.append('FE')
	resultado.append([reslin[0],reslin[1],reslin[2],reslin[3],reslin[4],reslin[5],reslin[6],reslin[7],reslin[8],reslin[9]])
cont = 1
with open('resultadopy.csv','w') as archivo:
	for v in resultado:
		archivo.write("Producto"+str(cont)+"\n")
		cont = cont+1
		est = 0
		for x in range(1,11):
			archivo.write("F"+str(x))
			if est != 9:
				archivo.write(",")
			est = est+1
		archivo.write("\n")
		est = 0
		for e in v:
			archivo.write(e)
			if est != 9:
				archivo.write(",")
			est = est+1
		archivo.write("\n")
archivo.close()
o = int(input("¿Abrir archivo?\n[1] Sí\t[2] No\n[ ]\b\b"))
if o == 1:
	print("Abriendo archivo...")
	os.system("resultadopy.csv")
