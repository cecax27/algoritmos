import os

class UI:
	errores = {1: "Opción no encontrada", 2: "Archivo no encontrado", 3: "Archivo con formato incorrecto", 4: "ID no encontrado", 5: "Número introducido fuera de rango"}

	def encabezado(self, string):
		os.system("cls")
		print("------------------------\n")
		print(" Logistic Beach Manager\n")
		print("------------------------\n")
		print("  "+string+"\n")

	def error(self, num):
		print("\n\n####")
		print("Error "+str(num))
		print(str(self.errores[num]))
		os.system("pause")

	def info(self):
		self.encabezado("Información")
		print("Programa hecho por:")
		print("Carlos Cervantes Aguilar")
		print("Estudiante de Ing. Mecatrónica")
		print("Código 214717889\n")
		print("Universidad de Guadalajara")
		print("Centro Universitario de los Lagos\n")
		print("Práctica 5 de Algoritmos y Estructuras de Datos")
		print("Profesor Víctor Becerra Córdoba\n")
		os.system("pause")

class Camion:
	clave = ""
	nombre = ""
	calificaciones = []

def buscar():
	interface.encabezado("Buscar camión")
	clave = input("ID de camión:")
	band = False
	for buscador in registro:
		if buscador.clave == clave:
			band = True
			print("\n\nID: ["+str(buscador.clave)+"]")
			print("Nombre: "+str(buscador.nombre))
			print("\nCalificaciones:\n")
			i = 1
			for a in buscador.calificaciones:
				print("Viaje "+str(i)+": "+str(a))
				i = i + 1
			promedio = 0
			for a in buscador.calificaciones:
				promedio = promedio + a
			promedio = promedio//20
			print("\nCalificación promedio: "+str(promedio))
			print("Calificación más baja: "+str(min(buscador.calificaciones)))
			print("Calificación más alta: "+str(max(buscador.calificaciones)))
			os.system("pause")
			return True

	if not band:
		interface.error(4)

def promedios():
	interface.encabezado("Procesar promedios")
	print("i. Está función procesa la información de "+str(len(registro))+" camiones, y guarda en la misma carpeta el archivo \"promedio-camion.csv\" con los resultados.\n")
	print("¿Guardar resultados?")
	print("[1] Sí")
	print("[2] No")
	o = int(input("[ ]\b\b"))
	if o == 1:
		print("\nCreando archivo...")
		with open("promedio-camion.csv",'w') as archivo:
			archivo.write("id,nombreCamion,promedio\n")
			for a in registro:
				suma = 0
				for b in a.calificaciones:
					suma = suma + b
				suma = suma / 20
				archivo.write(str(a.clave)+","+str(a.nombre)+","+str(suma)+"\n")
		archivo.close()
		print("\nAbriendo archivo...")
		os.system("promedio-camion.csv")

def modificar(modo):
	if modo == 1:
		interface.encabezado("Modificar resultados")
		clave = input("ID de camión:")
		band = False
		for buscador in registro:
			if buscador.clave == clave:
				band = True
				print("\n\nID: ["+str(buscador.clave)+"]")
				print("Nombre: "+str(buscador.nombre))
				print("\nCalificaciones:\n")
				i = 1
				for a in buscador.calificaciones:
					print("Viaje "+str(i)+": "+str(a))
					i = i + 1
				
				viaje = int(input("Número de viaje: "))
				if viaje > 20 or viaje < 1:
					interface.error(5)
					return False
				cal = int(input("Calificación: "))
				if cal > 10 or cal < 1:
					interface.error(5)
					return False
				buscador.calificaciones[viaje-1] = cal
				
				print("Modificación realizada.")
				os.system("pause")
				return True

		if not band:
				interface.error(4)
	elif modo == 2:
		interface.encabezado("Modificar resultados")
		clave = input("ID de camión:")
		band = False
		for buscador in registro:
			if buscador.clave == clave:
				band = True
				print("\n\nID: ["+str(buscador.clave)+"]")
				print("Nombre: "+str(buscador.nombre))
				print("\nCalificaciones:\n")
				i = 1
				for a in buscador.calificaciones:
					print("Viaje "+str(i)+": "+str(a))
					i = i + 1
				for x in range(0,20):
					t = False
					while not t:
						cal = int(input("Calificación "+str(x+1)+" :"))
						if cal > 10 or cal < 1:
							interface.error(5)
						else:
							t = True
					buscador.calificaciones[x] = cal
				
				print("Modificación realizada.")
				os.system("pause")
				return True

		if not band:
				interface.error(4)
	else:
		interface.error(1)

def leer_datos(nombre):
	try:
		with open(nombre,"r") as archivo:
			try:
				datos = archivo.read().splitlines()
				datos.pop(0)
				for linea in datos:
					dat = linea.split(',')

					new = Camion()
					new.clave = dat[0]
					new.nombre = dat[1]
					dat.pop(0)
					dat.pop(0)
					aux = []
					for x in dat:
						aux.append(int(x))
					new.calificaciones = aux
					registro.append(new)

				print("\nDatos leidos correctamente.")
				print(str(len(registro))+" camiones registrados.\n")
				os.system("pause")
			except:
				interface.error(3)
				return False
		archivo.close()
		return True
	except:
		interface.error(2)
		return False

def escribir_datos(nombre):
	with open(nombre,'w') as archivo:
		archivo.write("id,nombreCamion,calViaje1,calViaje2,calViaje3,calViaje4,calViaje5,calViaje6,calViaje7,calViaje8,calViaje9,calViaje10,calViaje11,calViaje12,calViaje13,calViaje14,calViaje15,calViaje16,calViaje17,calViaje18,calViaje19,calViaje20\n")
		for a in registro:
			viajes = ""
			for b in a.calificaciones:
				viajes = viajes + "," + str(b)
			archivo.write(str(a.clave)+","+str(a.nombre)+str(viajes)+"\n")
	archivo.close()

def salir():
	print("Saliendo...")

	os.system("cls")

def menu():
	nivel = 0
	op = -1
	salida = 0
	while op != salida: #Antes de leer el archivo
		if nivel == 0:
			interface.encabezado("Menu principal")
			print("[1] Leer datos desde archivo .csv")
			print("[2] Información")
			print("\n[0] Salir")
			op = int(input("\n[ ]\b\b"))

			if op == 1:
				interface.encabezado("Leer datos")
				print("i. El archivo .csv debe estar en la misma carpeta que este programa.")
				nombre = str(input("\nNombre del archivo: "))+".csv"
				while not leer_datos(nombre):
					interface.encabezado("Leer datos")
					print("i. El archivo .csv debe estar en la misma carpeta que este programa.")
					nombre = str(input("\nNombre del archivo: "))+".csv"
				nivel = 1
			elif op == 2:
				interface.info()
			elif op == 0:
				salir()
			else:
				interface.error(1)

		elif nivel == 1: #Después de leer el archivo
			interface.encabezado("Menu principal")
			print("[1] Buscar camión")
			print("[2] Obtener promedios\n")
			print("[3] Modificar calificaciones\n")
			print("[4] Volver a leer datos")
			print("\n[0] Salir")
			op = int(input("\n[ ]\b\b"))

			if op == 1:
				buscar()
			elif op == 2:
				promedios()
			elif op == 3:
				interface.encabezado("Modificar calificaciones")
				print("[1] Modificar resultados de un viaje")
				print("[2] Modificar resultados de todo los viajes")
				modo = int(input("\n[ ]\b\b"))
				if modificar(modo):
					escribir_datos(nombre)
			elif op == 4:
				registro.clear()
				interface.encabezado("Leer datos")
				print("i. El archivo .csv debe estar en la misma carpeta que este programa.")
				nombre = str(input("\nNombre del archivo: "))+".csv"
				while not leer_datos(nombre):
					interface.encabezado("Leer datos")
					print("i. El archivo .csv debe estar en la misma carpeta que este programa.")
					nombre = str(input("\nNombre del archivo: "))+".csv"
			elif op == 0:
				salir()
			else:
				interface.error(1)

nombre = ""
interface = UI()
registro = []
menu()