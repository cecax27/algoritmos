/*
La empresa Robotic�s Mex, realiza un proceso industrial en el cual un robot ejecuta el viaje en las 10 estaciones de 
la planta, en cada viaje se transporta un producto, el cual se va modificando en cada estaci�n, por lo cual, cada 
estaci�n le informa con un numero sobre los resultados del proceso, este n�mero puede ser entre 0 o 1000, por lo 
cual deber� leer todos los valores que fueron entregados en cada estaci�n durante el ciclo de trabajo de 8 horas, 
dicho ciclo abarca 10,000 operaciones, por lo tanto, se le entrega un archivo CSV donde se tienen almacenados los 
valores de cada vuelta del proceso.

Cada valor tiene un significado con base a este rango

�         0 a 200 = Fase fallida, debe canalizarse a material reciclado = FMR

�         201 a 500 = Fase en condiciones medias = FM

�         501 a 799 = Fase en condiciones altas = FA

�         800 a 1000 = Fase en condiciones excelentes con = FE

Genere un archivo nuevo en el cual indique el resultado de cada fase. Utilice arreglos para realizar las 
comparaciones, es decir, primero debe leer todo el archivo y guardarlo en un arreglo, para despu�s generar las 
comparaciones y almacenar en un nuevo archivo

Programa hecho por Carlos Cervantes Aguilar
Materia Algoritmos y estructuras de datos
C�digo 214717889*/

#include <iostream>
#include <fstream>

using namespace std;

#define vueltas 10000
#define estaciones 10

int datos[vueltas][estaciones];

void stringtoint(string cadena, int vuelta){
	int i,j,k,digitos=0,valor=0,unidad=1,estacion=0;
	string num,vacio;
	for(i=0;i<cadena.length();i++){
		if(cadena[i]!=','){
			num[digitos]=cadena[i];
			digitos++;
		}
		else{
			for(k=digitos-1;k>=0;k--){
				valor+=(num[k]-'0')*unidad;
				unidad*=10;
			}
			datos[vuelta][estacion]=valor;
			estacion++;
			valor=0;
			digitos=0;
			unidad=1;
			num = vacio; 
		}
	}
	for(k=digitos-1;k>=0;k--){
				valor+=(num[k]-'0')*unidad;
				unidad*=10;
			}
			datos[vuelta][estacion]=valor;
}

bool leerdatos(){
	ifstream archivo;
	string nombre;
	cout<<"Nombre del archivo .csv: ";
	cin>>nombre;
	nombre = nombre + ".csv";
	archivo.open(nombre.c_str());
	//---------------------------------------
	if(archivo.fail()){
		cout<<"El archivo no existe."<<endl;
		return false;
	}
	//---------------------------------------
	int i,j,linea=0;
	string num;
	getline(archivo,num);
	for(i=0;i<vueltas;i++){
		getline(archivo,num);
		stringtoint(num,i);
		linea++;
	}
	cout<<linea<<" lineas leidas."<<endl;
	archivo.close();
	return true;
}

bool escribirdatos(){
	ofstream archivo;
	int i,j;
	archivo.open("resultado.csv");
	//---------------------------------------
	if(archivo.fail()){
		cout<<"El archivo no existe."<<endl;
		return false;
	}
	//---------------------------------------
	for(i=0;i<vueltas;i++){
		archivo<<"Producto"<<i+1<<endl;
		archivo<<"F1,F2,F3,F4,F5,F6,F7,F8,F9,F10"<<endl;
		for(j=0;j<estaciones;j++){
			if(datos[i][j]<=200)	archivo<<"FMR";
			else if(datos[i][j]<=500)	archivo<<"FM";
			else if(datos[i][j]<=799) archivo<<"FA";
			else if(datos[i][j]<=1000) archivo<<"FE";
			
			if(j!=9) archivo<<",";
			else archivo<<endl;
		}
	}
	archivo.close();
	cout<<"Archivo de resultados creado como resultado.csv"<<endl;
}

int main(){
	cout<<"------------------------"<<endl
		<<"      Robotic's Mex     "<<endl
		<<"------------------------"<<endl
		<<"   Procesar archivos   "<<endl
		<<"i. Este programa lee un archivo .csv, procesa los datos y guarda en la misma carpeta el archivo \"resultadopy.csv\" con la clasficiacion por clases.\n\n";
		
	while(leerdatos()==false){
	}
	
	escribirdatos();
	cout<<"Abrir archivos?"<<endl
		<<"[1] Si\t[2] No"<<endl
		<<"[ ]\b\b";
	int op;
	cin>>op;
	if(op == 1){
		cout<<"Abiendo archivo...";
		system("resultado.csv");
	}
}
