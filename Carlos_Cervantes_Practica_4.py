import os
from operator import itemgetter
datos = []
resultado = []
def leer():
	os.system("cls")
	nombre = input("Nombre del documento original: ")
	print("Leyendo...\n")
	nombre = nombre + ".csv"
	with open(nombre,'r') as archivo:
		lineas=archivo.read().splitlines()
		lineas.pop(0)
		for l in lineas:
			linea = l.split(',')
			for d in linea:
				datos.append(int(d))
	archivo.close();
	for e in datos:
		if e <=200:
			resultado.append('FMR')
		elif e <=500:
			resultado.append('FM')
		elif e <=799:
			resultado.append('FA')
		elif e <=1000:
			resultado.append('FE')
	print(str(len(datos))+" datos leídos correctamente.")
	os.system("pause")

def ordenamientoX(modo):
	copia = datos
	os.system("cls")
	print("Procesando...\n")
	print(" 0%")
	for x in range(len(copia)-1,0,-1):
		if (x%1000)==0 :
			os.system("cls")
			print("Procesando...\n")
			print(" "+str(100-(x//1000))+"%")
		for y in range(x):
			if copia[y] > copia[y+1]:
				aux = copia[y]
				copia[y] = copia[y+1]
				copia[y+1] = aux

	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("Ordenamiento burbuja\n")
		print(copia)
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("Ordenamiento burbuja\n")
			archivo.write("Número ordenado\n")
			for x in range(0,len(copia)):
				archivo.write(str(copia[x])+"\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def ordenamientoY(modo):
	copia = datos
	for x in range(0,len(copia)):
		if (x%1000)==0 :
			os.system("cls")
			print("Procesando...\n")
			print(" "+str(x//1000)+"%")
		y = x
		aux = copia[x]
		while y > 0 and copia[y-1] > aux:
			copia[y] = copia[y-1]
			y = y-1
		copia[y] = aux

	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("Ordenamiento inserción\n")
		print(copia)
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("Ordenamiento inserción\n")
			archivo.write("Número ordenado\n")
			for x in range(0,len(copia)):
				archivo.write(str(copia[x])+"\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def ordenamientoZ(modo):
	copia = datos
	for x in range(0,len(copia)):
		if (x%1000)==0 :
			os.system("cls")
			print("Procesando...\n")
			print(" "+str(x//1000)+"%")
		y = x
		for z in range(x+1,len(copia)):
			if copia[z] < copia[y]:
				y = z
		aux = copia[x]
		copia[x] = copia[y]
		copia[y] = aux

	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("Ordenamiento selección\n")
		print(copia)
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("Ordenamiento selección\n")
			archivo.write("Número ordenado\n")
			for x in range(0,len(copia)):
				archivo.write(str(copia[x])+"\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def masFrecuentes(modo):
	os.system("cls")
	print("Procesando...")
	frecuentes = []
	for x in range(1,1001):
		frecuentes.append([x,datos.count(x)])
	frecuentes.sort(key=itemgetter(1),reverse=True)	
	frecuentes = frecuentes[0:100]
	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("100 números de error más frecuentes\n")
		for x in range(0,100):
			print(str(x+1)+". "+str(frecuentes[x][0]) + " con "+str(frecuentes[x][1])+" repeticiones\n")
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("100 números de error más frecuentes\n")
			archivo.write("Posición,Número de error,Repeticiones\n")
			for x in range(0,100):
				archivo.write(str(x+1)+","+str(frecuentes[x][0]) + ","+str(frecuentes[x][1])+"\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def menosFrecuentes(modo):
	os.system("cls")
	print("Procesando...")
	frecuentes = []
	for x in range(1,1001):
		frecuentes.append([x,datos.count(x)])
	frecuentes.sort(key=itemgetter(1))	
	frecuentes = frecuentes[0:100]
	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("100 números de error menos frecuentes\n")
		for x in range(0,100):
			print(str(x+1)+". "+str(frecuentes[x][0]) + " con "+str(frecuentes[x][1])+" repeticiones\n")
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("100 números de error más frecuentes\n")
			archivo.write("Posición,Número de error,Repeticiones\n")
			for x in range(0,100):
				archivo.write(str(x+1)+","+str(frecuentes[x][0]) + ","+str(frecuentes[x][1])+"\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def promedio(modo):
	suma = 0
	for i in datos:
		suma = suma + i
	promedio = suma // len(datos)

	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("Número promedio de error\n")
		print(str(promedio)+"\n")
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("Número promedio de error\n")
			archivo.write("Promedio,Total de errores\n")
			archivo.write(str(promedio)+","+str(len(datos)))
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def numeroFase(modo):
	fmr = resultado.count('FMR')
	fm = resultado.count('FM')
	fa = resultado.count('FA')
	fe = resultado.count('FE')
	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("Veces que se repitió cada fase\n")
		print("Fase fallida, debe canalizarse a material reciclado (FMR): "+str(fmr)+"\n")
		print("Fase en condiciones medias (FM): "+str(fm)+"\n")
		print("Fase en condiciones altas (FA): "+str(fa)+"\n")
		print("Fase en condiciones excelentes (FE): "+str(fe)+"\n")
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("Veces que se repitió cada fase\n")
			archivo.write("FMR,FM,FA,FE\n")
			archivo.write(str(fmr)+","+str(fm)+","+str(fa)+","+str(fe)+"\n")
			archivo.write("FMR,Fase fallida. debe canalizarse a material reciclado\n")
			archivo.write("FM,Fase en condiciones medias\n")
			archivo.write("FA,Fase en condiciones altas\n")
			archivo.write("FE,Fase en condiciones excelentes\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def faseFrecuente(modo):
	fases = []
	fases.append(resultado.count('FMR'))
	fases.append(resultado.count('FM'))
	fases.append(resultado.count('FA'))
	fases.append(resultado.count('FE'))
	fase = {}
	fase[0] = "FMR"
	fase[1] = "FM"
	fase[2] = "FA"
	fase[3] = "FE"
	if  modo == 1: #Modo imprimir
		os.system("cls")
		print("Fase con mayor presentación\n")
		print(str(fase[fases.index(max(fases))])+"\n")
		print("Se presentó "+str(max(fases))+" veces\n")
		os.system("pause")
	elif modo == 2: #Modo archivo
		nombre = input("Nombre del archivo de resultado: ")
		nombre = nombre + ".csv"
		with open(str(nombre),'w') as archivo:
			archivo.write("Archivo de resultados\n")
			archivo.write("Fase con mayor presentación\n")
			archivo.write("Fase,Veces que se presentó\n")
			archivo.write(str(fase[fases.index(max(fases))])+","+str(max(fases))+"\n")
		archivo.close()
		print("Abriendo archivo...")
		os.system(nombre)

def salir():
	print("Saliendo...")
	os.system("cls")

def menu():
	op = 0
	leer()
	while op != 1:
		os.system("cls")
		print("Menu principal\n\n")
		print("1. Salir\n")
		print("2. Guardar resultados\n")
		print("3. Ordenamiento burbuja")
		print("4. Ordenamiento inserción")
		print("5. Ordenamiento selección")
		print("6. 100 errores más frecuentes")
		print("7. 100 errores menos frecuentes")
		print("8. Número promedio de error")
		print("9. Número de veces de cada fase")
		print("10. Fase con mayor presentación")
		op = int(input("-> "))
		if op == 1:
		 	salir()
		elif op == 2:
			os.system("cls")
			print("Guardar resultados\n\n")
			print("1. Ordenamiento burbuja")
			print("2. Ordenamiento inserción")
			print("3. Ordenamiento selección")
			print("4. 100 errores más frecuentes")
			print("5. 100 errores menos frecuentes")
			print("6. Número promedio de error")
			print("7. Número de veces de cada fase")
			print("8. Fase con mayor presentación")
			op1 = int(input("-> "))
			if op1 == 1:
				ordenamientoX(2)
			elif op1 == 2:
				ordenamientoY(2)
			elif op1 == 3:
				ordenamientoZ(2)
			elif op1 == 4:
				masFrecuentes(2)
			elif op1 == 5:
				menosFrecuentes(2)
			elif op1 == 6:
				promedio(2)
			elif op1 == 7:
				numeroFase(2)
			elif op1 == 8:
				faseFrecuente(2)
		elif op == 3:
			ordenamientoX(1)
		elif op == 4:
			ordenamientoY(1)
		elif op == 5:
			ordenamientoZ(1)
		elif op == 6:
			masFrecuentes(1)
		elif op == 7:
			menosFrecuentes(1)
		elif op == 8:
			promedio(1)
		elif op == 9:
			numeroFase(1)
		elif op == 10:
			faseFrecuente(1)

menu()